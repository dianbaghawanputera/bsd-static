/**
 * imports
 */
import '@/css/index.css'
import { lazyLoadSection, detectFirstScroll } from './js/helper.js'

import 'tw-elements';

detectFirstScroll()
lazyLoadSection()