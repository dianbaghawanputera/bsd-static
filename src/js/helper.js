/**
* Detect first scroll and set class to header
*/
export function detectFirstScroll(){
	const header = document.getElementById('header');
	
	window.addEventListener("scroll", function(){
		var st = window.pageYOffset || document.documentElement.scrollTop;
		if (st > 0){
			// downscroll code
			header.classList.add('header-scrolled');
		} else {
			// upscroll code
			header.classList.remove('header-scrolled');
		}
	}, false);
}

export function lazyLoadSection(){
	const sections = document.querySelectorAll("[data-io]");
	
	if ("IntersectionObserver" in window) {
		const ioConfig = {
			threshold: 0.6
		};
		
		var observer = new IntersectionObserver(entries => {
			entries.forEach(entry => {
				var tpl = document.querySelector(
					`[data-template="${entry.target.dataset.io}"]`
					);
					var clon = tpl.content.cloneNode(true);
					
					if (entry.isIntersecting) {
						entry.target.appendChild(clon);
						observer.unobserve(entry.target);
					}
				});
			}, ioConfig);
			
			sections.forEach(section => {
				observer.observe(section);
			});
		}
	}