/**
* Tailwind CSS configuration file
*
* docs: https://tailwindcss.com/docs/configuration
* default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
*/
const path = require('path')

module.exports = {
    theme: {
        extend: {},
        container: {
            center: true,
            padding: {
                DEFAULT: '1.25rem',
                md: '2rem',
                xl: '1.25rem'
            }
        },

        screens: {
            'xs': '400px',
            'sm': '640px',
            'md': '768px',
            'lg': '1024px',
            'xl': '1200px',
            '2xl': '1600px'
        },

        fontFamily: {
            'sans': ['Inter', 'ui-sans-serif', 'system-ui', '-apple-system', 'Helvetica Neue', 'Arial', 'sans-serif'],
            'serif': ['ui-serif', 'Georgia', 'Cambria', 'Times New Roman', 'Times', 'serif']
        },

        colors: {
            transparent: 'transparent',
            current: 'currentColor',
            'white': '#ffffff',
            'black': '#000',
            'blue': {
                1: '#1c4271',
                2: '#26a9e0',
                3: '#00d7d1'
            }
        },

        fontSize: {
            'xs': ['.75rem', '1.3'],
            'sm': ['.875rem', '1.3'],
            'tiny': ['.875rem', '1.3'],
            'base': ['1rem', '1.65'],
            'lg': ['1.125rem', '1.65'],
            'xl': ['1.25rem', '1.65'],
            '2xl': ['1.5rem', '1.4'],
            '3xl': ['1.875rem', '1.4'],
            '4xl': ['2.25rem', '1.1'],
            '5xl': ['3rem', '1'],
            '6xl': ['4rem', '1'],
            '7xl': ['5rem', '1'],
        },

        fontWeight: {
            normal: 400,
            medium: 500,
            bold: 700,
        },
    },
    content: [
        path.resolve(__dirname, '**/*.{js,html}')
    ]
}
